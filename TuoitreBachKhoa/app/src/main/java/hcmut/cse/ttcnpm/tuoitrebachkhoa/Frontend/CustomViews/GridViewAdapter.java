package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

/**
 * Created by duythai on 2/26/2016.
 */

public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList data = new ArrayList();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) row.findViewById(R.id.titleTV);
            holder.image = (ImageView) row.findViewById(R.id.imageIV);
            holder.content = (TextView) row.findViewById(R.id.contentTV);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = (ImageItem) data.get(position);
        holder.title.setText(item.getTitle());
        holder.image.setImageBitmap(item.getImage());
        holder.content.setText(item.getContent());

        return row;
    }

    static class ViewHolder {
        TextView title;
        TextView content;
        ImageView image;
    }

    static public class ImageItem {
        private Bitmap image;
        private String title;
        private String content;

        public ImageItem(Bitmap image, String title, String content) {
            super();
            this.image = image;
            this.title = title;
            this.content = content;
        }

        public Bitmap getImage() {
            return image;
        }

        public void setImage(Bitmap image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
