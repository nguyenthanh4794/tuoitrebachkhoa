package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.CASLoginRequest;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private final String urlLogin = "https://sso.hcmut.edu.vn/cas/login?service=http://test.tuoitrebachkhoa.edu.vn";

    EditText _emailText, _passwordText;
    Button _loginButton;
    TextView _signupLink, _getNewPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        _emailText = (EditText) findViewById(R.id.input_email);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _loginButton = (Button) findViewById(R.id.btn_login);
        _signupLink = (TextView) findViewById(R.id.link_signup);
        _getNewPassword = (TextView) findViewById(R.id.link_getnewpassword);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            _emailText.setText(extras.getString("EMAIL"));
            _passwordText.setText(extras.getString("PASSWORD"));
        }

        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start the RegisterActivity
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                intent.putExtra("EMAIL", _emailText.getText().toString());
                intent.putExtra("PASSWORD", _passwordText.getText().toString());
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        _getNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start the ForgotenPasswordActivity
                Intent intent = new Intent(getApplicationContext(), ForgotenPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignInActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        final String email = _emailText.getText().toString();
        final String password = _passwordText.getText().toString();

        final CASLoginRequest casLoginRequest = new CASLoginRequest("https://sso.hcmut.edu.vn/cas/",
                "http://test.tuoitrebachkhoa.edu.vn", email, password);

        new android.os.Handler().postDelayed(
            new Runnable() {
                @Override
                public void run() {
                    String a = "3";
                    if (casLoginRequest.login())
                        onLoginSuccess();
                    else onLoginFailed();

                    progressDialog.dismiss();
                }
            }, 3000);

//        final WebView webView = new WebView(this);
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setDomStorageEnabled(true);
//        webSettings.setJavaScriptEnabled(true);
//        webView.loadUrl(urlLogin);
//
//        webView.setWebViewClient(new WebViewClient() {
//            public void onPageFinished(final WebView view, String url) {
//
//                view.loadUrl("javascript: { var androidUsername = document.getElementById('username').value='" + email +
//                        "'; var androidPassword = document.getElementById('password').value='" + password +
//                        "'; var button = document.getElementsByName('submit')[0].click(); }");
//                new android.os.Handler().postDelayed(
//                        new Runnable() {
//                            @Override
//                            public void run() {
//                                if (webView.getUrl().equals(urlLogin))
//                                    onLoginFailed();
//                                else {
//                                    onLoginSuccess();
//                                }
//                                progressDialog.dismiss();
//                            }
//                        }, 1000);
//            }
//        });
    }

    private boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty()) {
            _emailText.setError("enter a valid MSSV");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 16) {
            _passwordText.setError("between 4 and 16 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Sai MSSV hoặc mật khẩu", Toast.LENGTH_SHORT).show();

        _loginButton.setEnabled(true);
    }

    private void onLoginSuccess() {
        _loginButton.setEnabled(true);

        //TODO: After login successfully

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra("MSSV", _emailText.getText().toString());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }
}
