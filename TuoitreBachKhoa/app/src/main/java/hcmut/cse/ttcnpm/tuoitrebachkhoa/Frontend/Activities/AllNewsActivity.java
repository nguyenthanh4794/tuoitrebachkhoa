package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews.GridViewAdapter;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.HttpGetJsonRequest;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.ImageLoadTask;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.MainActivity;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class AllNewsActivity extends AppCompatActivity {

    private  static  final  String server = "http://49.213.118.219:8001/";
    static ListView listView;
    private static final String TAG_ID = "_id";
    //__v
    private static final String TAG_IMAGE = "image";
    private static final String TAG_CENSORED = "censorred";
    private static final String TAG_CMD = "cmd";
    //Tag
    private static final String TAG_CREATED_AT = "createdAt";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_CONTENT = "content";
    private static final String TAG_CATEGORY_NAME = "category_name";
    private static final String TAG_CATEGORY = "category";
    //CODE
    private static final String TAG_AUTHOR = "author";
    private static final String TAG_TITLE = "title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_news);

        List<Map<String, String>> data2disp = new ArrayList<Map<String, String>>();
        HttpGetJsonRequest r = new HttpGetJsonRequest();
        listView = (ListView)findViewById(R.id.listNews2);
        r.execute("http://49.213.118.219:8001/allnews");
        final ProgressDialog progressDialog = new ProgressDialog(AllNewsActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");

        while (r == null || r.result == null || r.result.toString() == "")
        {
            try {
                progressDialog.show();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        progressDialog.dismiss();
        final String jsonStr = r.result.toString();
        ArrayList<HashMap<String,String>> listNews = new ArrayList<HashMap<String,String>>();
        ArrayList<GridViewAdapter.ImageItem> imageItems =  new ArrayList<>();
        try {
            try {
                JSONArray arrayNews = new JSONArray(jsonStr);

                // looping through All Contacts
                for (int i = 0; i < 10; i++) {
                    JSONObject c = arrayNews.getJSONObject(i);
                    String _id = c.getString(TAG_ID);
                    String _categoryName = c.getString(TAG_CATEGORY_NAME);
                    String _category = c.getString(TAG_CATEGORY);
                    String _newsTitle = c.getString(TAG_TITLE);
                    String _newsDescription = c.getString(TAG_DESCRIPTION);
                    String _newsContent = c.getString(TAG_CONTENT);
                    String _image = c.getString(TAG_IMAGE);
                    boolean _censorred = c.getBoolean(TAG_CENSORED);
                    boolean _cmd = c.getBoolean(TAG_CMD);
                    //JSONObject _tag = c.getJSONObject("tag");
                    String _createdAt = c.getString(TAG_CREATED_AT);
                    //code
                    String _author = c.getString(TAG_AUTHOR);

                    // tmp hashmap for single news
                    HashMap<String, String> news = new HashMap<String, String>();
                    news.put(TAG_CATEGORY_NAME,_categoryName);
                    news.put(TAG_DESCRIPTION,_newsDescription);
                    news.put(TAG_CONTENT,_newsContent);
                    news.put(TAG_TITLE,_newsTitle);
                    news.put(TAG_IMAGE,_image);;

                    getBitmapFromURL bm = new getBitmapFromURL();
                    bm.execute(server + _image);
//                    progressDialog = new ProgressDialog(AllNewsActivity.this, R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Load Data...");

                    while (bm == null || bm.result == null)
                    {
                        try {
                            progressDialog.show();
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    Bitmap image =bm.result;
                    _newsTitle = _newsTitle.length() > 60 ? _newsTitle.substring(0,60)  + "...": _newsTitle;
                    _newsDescription = _newsDescription.length() > 100 ? _newsDescription.substring(0,100) + "...": _newsDescription;

                    imageItems.add(new GridViewAdapter.ImageItem(image, _newsTitle, _newsDescription));

                    //adding news to listNews
                    listNews.add(news);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }finally {

        }

        GridViewAdapter gridAdapter;
        gridAdapter = new GridViewAdapter(this, R.layout.allnews, imageItems);
        listView.setAdapter(gridAdapter);
        progressDialog.dismiss();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                JSONArray arrayNews = null;//ong lam di
                String _id = "";
                try {
                    arrayNews = new JSONArray(jsonStr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject c = null;
                try {
                    c = arrayNews.getJSONObject(position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                     _id = c.getString(TAG_ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Toast.makeText(AllNewsActivity.this, _id, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AllNewsActivity.this, TinTucActivity.class);
                intent.putExtra("link", server+"fullNews/"+_id);
//
//                //Start details activity
                startActivity(intent);
            }
        });
    }
        class getBitmapFromURL extends AsyncTask<String, Void, Void> {
        public Bitmap result = null;
        @Override
        protected Void doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                result = BitmapFactory.decodeStream(input);
                result = Bitmap.createScaledBitmap(result, 128, 128, true);
                return null;
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void Result){
            //TODO Read result and parse to JsonObject
            super.onPostExecute(Result);
        }
    }
}