package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.ImageLoadTask;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

/**
 * Created by NguyenThanh on 2/26/2016.
 */
public class CustomListEvents extends BaseAdapter {

    private static final String TAG_ID = "_id";
    //__v
    private static final String TAG_NAME = "name";
    private static final String TAG_LOCATION = "location";
    private static final String TAG_START_AT = "startAt";
    //Tag
    private static final String TAG_END_AT = "endAt";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_CONTENT = "content";
    private static final String TAG_FACULTY = "faculty";
    private static final String TAG_FACULTY_NAME = "faculty_name";
    //CODE
    private static final String TAG_IMAGE = "img";
    private static final String TAG_IS_ACTIVITY = "isActivity";
    private static final String TAG_PARTICIPANT = "participant";

    private ArrayList<HashMap<String,String>> listNews;
    private Context context;

    public CustomListEvents(Context c, ArrayList<HashMap<String,String>> listNews) {
        this.listNews = listNews;
        this.context = c;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View list;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            list = new View(context);
            list = inflater.inflate(R.layout.event_item, null);
            ImageView imageView = (ImageView) list.findViewById(R.id.list_image);
            TextView name = (TextView) list.findViewById(R.id.name);
            TextView description = (TextView) list.findViewById(R.id.description);
            TextView startAt = (TextView) list.findViewById(R.id.startAt);
            TextView endAt = (TextView) list.findViewById(R.id.endAt);

            //Assign value
            new ImageLoadTask(listNews.get(position).get(TAG_IMAGE), imageView).execute();
            String _name = listNews.get(position).get(TAG_NAME);
            String _description = listNews.get(position).get(TAG_DESCRIPTION);
            if(_name.length() > 60) {
                _name = _name.substring(0, 52);
                _name += "...";
            }
            if(_description.length() > 100) {
                _description = _description.substring(0,97);
                _description += "...";
            }
            name.setText(_name);
            description.setText(_description);
            String start = listNews.get(position).get(TAG_START_AT);
            String end = listNews.get(position).get(TAG_END_AT);
            startAt.setText("Bắt đầu: " + start.substring(0, 10));
            endAt.setText("Kết thúc: " + end.substring(0,10));
        } else {
            list = (View) convertView;
        }

        return list;
    }

    @Override
    public int getCount() {
        return listNews.size();
    }
}