package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

        import android.app.ProgressDialog;
        import android.content.Intent;
        import android.graphics.Typeface;
        import android.os.Bundle;
        import android.support.design.widget.FloatingActionButton;
        import android.support.design.widget.Snackbar;
        import android.support.v7.app.AppCompatActivity;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ListView;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.HashMap;

        import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews.CustomListEvents;
        import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.HttpGetJsonRequest;
        import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class AllEventsActivity extends AppCompatActivity {

    private ListView lstView;
    private String url = "http://49.213.118.219:8001/event";

    private static final String TAG_ID = "_id";
    //__v
    private static final String TAG_NAME = "name";
    private static final String TAG_LOCATION = "location";
    private static final String TAG_START_AT = "startAt";
    //Tag
    private static final String TAG_END_AT = "endAt";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_CONTENT = "content";
    private static final String TAG_FACULTY = "faculty";
    private static final String TAG_FACULTY_NAME = "faculty_name";
    //CODE
    private static final String TAG_IMAGE = "img";
    private static final String TAG_IS_ACTIVITY = "isActivity";
    private static final String TAG_PARTICIPANT = "participant";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_events);

        HttpGetJsonRequest r = new HttpGetJsonRequest();
        r.execute(url);
        final ProgressDialog progressDialog = new ProgressDialog(AllEventsActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();

        while (r == null || r.result == null || r.result.toString() == "") {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        progressDialog.dismiss();
        String jsonStr = r.result.toString();
        final ArrayList<HashMap<String, String>> listNews = new ArrayList<HashMap<String, String>>();
        try {
            try {
                JSONArray arrayNews = new JSONArray(jsonStr);

                // looping through All Contacts
                for (int i = 0; i < arrayNews.length(); i++) {
                    JSONObject c = arrayNews.getJSONObject(i);
                    String _id = c.getString(TAG_ID);
                    String _name = c.getString(TAG_NAME);
                    String _location = c.getString(TAG_LOCATION);
                    String _startAt = c.getString(TAG_START_AT);
                    String _endAt = c.getString(TAG_END_AT);
                    String _description = c.getString(TAG_DESCRIPTION);
                    String _content = c.getString(TAG_CONTENT);
                    String _image = c.getString(TAG_IMAGE);

                    // tmp hashmap for single news
                    HashMap<String, String> news = new HashMap<String, String>();
                    news.put(TAG_NAME, _name);
                    news.put(TAG_DESCRIPTION, _description);
                    news.put(TAG_ID, _id);
                    news.put(TAG_LOCATION, _location);
                    news.put(TAG_IMAGE, _image);
                    news.put(TAG_START_AT, _startAt);
                    news.put(TAG_END_AT, _endAt);

                    //adding news to listNews
                    listNews.add(news);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } finally {

        }


        CustomListEvents adapter = new CustomListEvents(AllEventsActivity.this, listNews);
        lstView = (ListView) findViewById(R.id.listView);
        lstView.setAdapter(adapter);

        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Create intent
                Intent intent = new Intent(AllEventsActivity.this, EventActivity.class);
                intent.putExtra("NAME", listNews.get(position).get(TAG_NAME));
                intent.putExtra("DESCRIPTION", listNews.get(position).get(TAG_DESCRIPTION));
                intent.putExtra("ID", listNews.get(position).get(TAG_ID));
                intent.putExtra("START_AT", listNews.get(position).get(TAG_START_AT));
                intent.putExtra("END_AT", listNews.get(position).get(TAG_END_AT));
                intent.putExtra("LOCATION", listNews.get(position).get(TAG_LOCATION));
                intent.putExtra("IMAGE", listNews.get(position).get(TAG_IMAGE));
                //Start details activity
                startActivity(intent);
            }
        });
    }
}

