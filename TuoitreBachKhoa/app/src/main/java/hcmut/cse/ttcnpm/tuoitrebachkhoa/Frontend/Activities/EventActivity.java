package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.HttpGetJsonRequest;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.ImageLoadTask;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class EventActivity extends AppCompatActivity {
    private String url = "http://49.213.118.219:8001/fullEvent/";
    private TextView _name, _content, _description, _startAt, _endAt, _location;
    private ImageView _image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        _name = (TextView) findViewById(R.id.name);
        _description = (TextView) findViewById(R.id.description);
        _content = (TextView) findViewById(R.id.content);
        _startAt = (TextView) findViewById(R.id.startAt);
        _endAt = (TextView) findViewById(R.id.endAt);
        _location = (TextView) findViewById(R.id.location);
        _image = (ImageView) findViewById(R.id.image);

        Bundle extras = getIntent().getExtras();

        HttpGetJsonRequest r = new HttpGetJsonRequest();

        r.execute(url + extras.getString("ID"));

        final ProgressDialog progressDialog = new ProgressDialog(EventActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();

        while (r == null || r.result == null || r.result.toString() == "")
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        progressDialog.dismiss();

        String jsonStr = "[" + r.result.toString() + "]";
        JSONArray arrayNews = null;
        JSONObject c = null;
        try {
            arrayNews = new JSONArray(jsonStr);

            c = arrayNews.getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (extras != null) {
            _name.setText(extras.getString("NAME"));
            try {
                _content.setText(Html.fromHtml(c.getString("content")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            _description.setText(extras.getString("DESCRIPTION"));
            new ImageLoadTask(extras.getString("IMAGE"),_image).execute();
            _endAt.setText("Kết thúc: " + extras.getString("END_AT").substring(0,10));
            _startAt.setText("Bắt đầu: " + extras.getString("START_AT").substring(0,10));
            _location.setText("Địa điểm: " + extras.getString("LOCATION"));
        }
    }

}
