package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews;

/**
 * Created by ViVi on 2/26/2016.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.ImageLoadTask;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

/**
 * Created by NguyenThanh on 2/26/2016.
 */
public class CustomListNews extends BaseAdapter {

    private static final String TAG_ID = "_id";
    //__v
    private static final String TAG_TITLE = "title";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_IMAGE = "image";
    //CODE

    private ArrayList<HashMap<String,String>> listNews;
    private Context context;

    public CustomListNews(Context c, ArrayList<HashMap<String,String>> listNews) {
        this.listNews = listNews;
        this.context = c;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View list;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            list = new View(context);
            list = inflater.inflate(R.layout.allnews, null);
            ImageView imageView = (ImageView) list.findViewById(R.id.imageIV);
            TextView title = (TextView) list.findViewById(R.id.titleTV);
            TextView description = (TextView) list.findViewById(R.id.contentTV);

            //Assign value
            new ImageLoadTask(listNews.get(position).get(TAG_IMAGE), imageView).execute();
            String _title = listNews.get(position).get(TAG_TITLE);
            String _description = listNews.get(position).get(TAG_DESCRIPTION);
            title.setText(_title);
            description.setText(_description);
        } else {
            list = (View) convertView;
        }

        return list;
    }

    @Override
    public int getCount() {
        return listNews.size();
    }
}
