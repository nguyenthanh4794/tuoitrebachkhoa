package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Created by NguyenThanh on 2/26/2016.
 */
public class CASLoginRequest {
    private String jSessionId;
    private URL redirectUrl;
    private String fullUrlText;
    private String username;
    private String password;

    private String readAllBufferedReader(InputStream in) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        String line;
        while((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();
        return stringBuilder.toString();
    }

    public CASLoginRequest(String casUrl, String serviceUrl, String username, String password) {
        this.fullUrlText = casUrl + "login?service=" + serviceUrl;
        this.username = username;
        this.password = password;
    }

    public boolean login() {
        boolean success = false;
        try {
            URL fullUrl = new URL(fullUrlText);
            URLConnection firstCasConnection = fullUrl.openConnection();

            String firstCasConnectionData = readAllBufferedReader(firstCasConnection.getInputStream());

            String jSessionId = firstCasConnection.getHeaderFields().get("Set-Cookie").get(0);
            jSessionId = jSessionId.substring(0, jSessionId.indexOf(";"));
            Pattern ltPattern = Pattern.compile("<input type=\"hidden\" name=\"lt\" value=\"(.+?)\" />");
            Pattern executionPattern = Pattern.compile("<input type=\"hidden\" name=\"execution\" value=\"(.+?)\" />");

            Matcher ltMatcher = ltPattern.matcher(firstCasConnectionData);
            if (!ltMatcher.find()) {
                throw new IOException("Wrong answer sent by the remote server !");
            }

            Matcher executionMatcher = executionPattern.matcher(firstCasConnectionData);
            if (!executionMatcher.find()) {
                throw new IOException("Wrong answer sent by the remote server !");
            }

            String lt = ltMatcher.group(1);
            String execution = executionMatcher.group(1);

            HttpURLConnection loginCasConnection = (HttpURLConnection) fullUrl.openConnection();
            loginCasConnection.setRequestProperty("Cookie", jSessionId);
            loginCasConnection.setDoOutput(true);
            loginCasConnection.setInstanceFollowRedirects(true);
            OutputStreamWriter out = new OutputStreamWriter(loginCasConnection.getOutputStream());
            out.write("username=" + username);
            out.write("&password=" + password);
            out.write("&lt=" + lt);
            out.write("&execution=" + execution);
            out.write("&_eventId=submit&submit_btn=LOGIN");
            out.close();

            if (loginCasConnection.getHeaderField("Location") == null) {
                throw new IllegalStateException("The server didn't sent a correct answer. Maybe your login is incorrect.");
            }

            redirectUrl = new URL(loginCasConnection.getHeaderField("Location"));
            success = true;
        }catch (Exception ex) {}
        return success;
    }

    public String getjSessionId() {
        return jSessionId;
    }

    public void setjSessionId(String jSessionId) {
        this.jSessionId = jSessionId;
    }

    public URL getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(URL redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
