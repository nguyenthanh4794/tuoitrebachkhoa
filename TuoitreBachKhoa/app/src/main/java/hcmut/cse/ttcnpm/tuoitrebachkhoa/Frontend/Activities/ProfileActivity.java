package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class ProfileActivity extends AppCompatActivity {
    private ImageView imageViewRound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Variable
        int link_image = R.drawable.lg_256x256;
        String email = "51203336@gmail.com",
                firstname = "Họ",
                lastname = "Tên",
                city = "Quê quán",
                sex = "Female",
                birth = "19/06/1994";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
        firstname = extras.getString("MSSV");
            email = extras.getString("MSSV") + "@hcmut.edu.vn";
        }

        //Quang's code

        //Constant Link imageView
        //Circle Shape
        imageViewRound=(ImageView)findViewById(R.id.imageView_round);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), link_image);
        imageViewRound.setImageBitmap(icon);

        ImageView imageUnder = (ImageView) findViewById(R.id.imageViewUnder);
        imageUnder.setImageResource(link_image);

        //Constant Text TextView
        TextView txtFullnameOntop = (TextView) findViewById(R.id.txt_fullname_ontop);
        txtFullnameOntop.setText(firstname);

        final TextView txtEmailOntop = (TextView) findViewById(R.id.txt_email_ontop);
        txtEmailOntop.setText(email);
        TextView txtBirthdateOntop = (TextView) findViewById(R.id.txt_birthdate_ontop);
        txtBirthdateOntop.setText(birth);

        final TextView txrEmail = (TextView) findViewById(R.id.txt_email);
        txrEmail.setText(email);

        TextView txtFirstname = (TextView) findViewById(R.id.txt_firstname);
        txtFirstname.setText(firstname);

        TextView txtLastname = (TextView) findViewById(R.id.txt_lastname);
        txtLastname.setText(lastname);

        TextView txtCity = (TextView) findViewById(R.id.txt_city);
        txtCity.setText(city);

        TextView txtSex = (TextView) findViewById(R.id.txt_sex);
        txtSex.setText(sex);

        TextView txtBirthdate = (TextView) findViewById(R.id.txt_birthdate);
        txtBirthdate.setText(birth);

        final Button btnSave = (Button) findViewById(R.id.btn_save);

//        final Button btnEdit = (Button) findViewById(R.id.btn_edit);
//        btnEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                txrEmail.setFocusable(true);
//                txrEmail.setFocusableInTouchMode(true);
//                txrEmail.setClickable(true);
//                txrEmail.setEnabled(true);
//
//                btnEdit.setVisibility(View.INVISIBLE);
//                btnSave.setVisibility(View.VISIBLE);
//            }
//        });
//
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                txrEmail.setFocusable(false);
//                txrEmail.setFocusableInTouchMode(false);
//                txrEmail.setClickable(false);
//                txrEmail.setEnabled(false);
//
//                btnEdit.setVisibility(View.VISIBLE);
//                btnSave.setVisibility(View.INVISIBLE);
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trang_nhat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
