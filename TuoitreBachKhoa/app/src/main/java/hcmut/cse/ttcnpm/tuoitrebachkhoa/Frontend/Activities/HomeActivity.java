package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.CustomViews.CustomGrid;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class HomeActivity extends AppCompatActivity {

    private String mssv;

    GridView grid;
    String[] web = {
            "Tài khoản",
            "Sự kiện",
            "Tin tức"
//            "Lịch công tác",
//            "Góc sinh viên",
//            "Nhịp sống Bách Khoa",
//            "Kết nối Bách Khoa",
//            "Văn bản - Tài liệu",
//            "Thành viên",
//            "Liên hệ"

    };
    int[] imageId = {
            R.drawable.user,
            R.drawable.calendar,
            R.drawable.gift
//            R.drawable.folder,
//            R.drawable.download,
//            R.drawable.document,
//            R.drawable.database,
//            R.drawable.home,
//            R.drawable.printer,
//            R.drawable.twitter

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Bundle extras = getIntent().getExtras();
        if(extras != null) mssv = extras.getString("MSSV");

        CustomGrid adapter = new CustomGrid(HomeActivity.this, web, imageId);
        grid = (GridView) findViewById(R.id.gridView);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                        intent.putExtra("MSSV", mssv.toString());
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(HomeActivity.this, AllEventsActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(HomeActivity.this, AllNewsActivity.class);
                        startActivity(intent2);
                        break;
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
