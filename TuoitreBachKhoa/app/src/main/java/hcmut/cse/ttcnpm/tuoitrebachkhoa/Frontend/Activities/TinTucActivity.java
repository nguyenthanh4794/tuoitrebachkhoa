package hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.HttpGetJsonRequest;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.Frontend.Tools.ImageLoadTask;
import hcmut.cse.ttcnpm.tuoitrebachkhoa.R;

public class TinTucActivity extends AppCompatActivity {
    private static final String TAG_IMAGE = "image";
    private static final String TAG_TITLE = "title";
    private static final String TAG_CONTENT = "content";
    private static final String TAG_DESCRIPTION = "description";
    String path = "http://49.213.118.219:8001";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tin_tuc);
        Bundle extras = getIntent().getExtras();
        String link = extras.getString("link");
        HttpGetJsonRequest r = new HttpGetJsonRequest();
        r.execute(link);
        final ProgressDialog progressDialog = new ProgressDialog(TinTucActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");

        while (r == null || r.result == null || r.result.toString() == "")
        {
            try {
                progressDialog.show();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        progressDialog.dismiss();
        String jsonStr = "["+r.result.toString()+ "]";

        try {
//            try {
                JSONArray arrayNews = new JSONArray(jsonStr);
//                 Toast.makeText(TinTucActivity.this, arrayNews.toString(), Toast.LENGTH_SHORT).show();

            // looping through All Contacts
                    JSONObject c = arrayNews.getJSONObject(0);


                    ImageView image = (ImageView)findViewById(R.id.fullnewsImageIV);
                    new ImageLoadTask(path +"/"+ c.getString(TAG_IMAGE), image).execute();

                    TextView title = (TextView) findViewById(R.id.fullnewsTitleTV);
                    title.setText(c.getString(TAG_TITLE));

//                Toast.makeText(TinTucActivity.this, c.getString(TAG_IMAGE), Toast.LENGTH_SHORT).show();
//
//
                TextView des = (TextView) findViewById(R.id.fullnewsDesTV);
                    des.setText(c.getString(TAG_DESCRIPTION));

                String str = c.getString(TAG_CONTENT);
                while(str.indexOf("\"/Up")> 0){
                    str = str.substring(0, str.indexOf("\"/Up")+1) + path + str.substring(str.indexOf("\"/Up")+1);
                }
                TextView content = (TextView) findViewById(R.id.fullnewsContentTV);
                    content.setText(Html.fromHtml(str));



            } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(TinTucActivity.this, "Loi", Toast.LENGTH_SHORT).show();

        }
//        }finally {
//
        }
//    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tin_tuc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
